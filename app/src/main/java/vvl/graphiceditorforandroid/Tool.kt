package vvl.graphiceditorforandroid


enum class Tool {
    BRUSH, RECTANGLE, OVAL, TEXT, ZOOM, TRIANGLE
}