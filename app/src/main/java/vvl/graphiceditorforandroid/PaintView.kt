package vvl.graphiceditorforandroid;

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import java.util.*

class PaintView//paintScreen = Paint()
(ctx: Context, attrs: AttributeSet) : View(ctx, attrs) {
    //defines what to draw
    private val mDrawPaths = LinkedList<Path>()
    //defines how to draw
    private val mDrawPaints = LinkedList<Paint>()

    //canvas - holding pen, holds your drawings
    //and transfers them to the view
    private var mDrawCanvas: Canvas? = null
    //canvas bitmap
    private var mCanvasBitmap: Bitmap? = null


    private val mDrawPathsRedo = LinkedList<Path>()
    private val mDrawPaintsRedo = LinkedList<Paint>()

    private var mCurrentBrushSize: Int = 3
    private var mCurrentColor: Int = Color.BLACK
    private var mTool: Tool = Tool.BRUSH
    private lateinit var mText: String

    private var drawPath: Path? = null
    private var drawPaint: Paint? = null

    private val sTouchTolerance = 4f
    private val sMaxScale = 5f
    private val sMinScale = 1f

    private val mScaleGestureDetector: ScaleGestureDetector
    private var mScaleFactor = sMinScale
    private var originX = 0f
    private var originY = 0f

    private var mX = 0f
    private var mY = 0f
    private var startX = 0f
    private var startY = 0f


    init {
        mScaleGestureDetector = ScaleGestureDetector(this.context, ScaleListener())

        drawPath = Path()
        drawPaint = Paint()
        drawPaint!!.color = mCurrentColor
        drawPaint!!.isAntiAlias = true  //сглаживание краёв
        drawPaint!!.strokeWidth = mCurrentBrushSize.toFloat()
        drawPaint!!.style = Paint.Style.STROKE
        //drawPaint!!.strokeJoin = Paint.Join.ROUND
        drawPaint!!.strokeCap = Paint.Cap.ROUND

    }

    override fun onDraw(canvas: Canvas) {
        println("onDraw")

        canvas.save()

        /*if (mTool == Tool.MOVE)canvas.translate(startX,startY)
        else*/
        canvas.translate(0f, 0f)
        //canvas.skew(startX,startY)
        //if (mTool == Tool.MOVE)canvas.translate(maxOf(originX,mX), maxOf(originY,mY))
        //else

        //println(" " + originX + " " + originY)
        canvas.translate(originX, originY)
        canvas.scale(mScaleFactor, mScaleFactor)

        canvas.drawBitmap(mCanvasBitmap, 0f, 0f, drawPaint)
        var i = 0
        while (mDrawPaths.size > i) {
            canvas.drawPath(mDrawPaths[i], mDrawPaints[i])
            i++
        }

        canvas.restore()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        //create canvas of certain device size.
        println("onsizechanged")
        originX = 0f
        originY = 0f
        if (mCanvasBitmap == null) {
            mCanvasBitmap = clearBitmap()
        }
        //create Bitmap of certain w,h
        mCanvasBitmap = Bitmap.createScaledBitmap(mCanvasBitmap, w, h, false)

        //mCanvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565)

        //mCanvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)

        //apply bitmap to graphic to start drawing.

        mDrawCanvas = Canvas(mCanvasBitmap)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val touchX = event.x
        val touchY = event.y

        if (event.pointerCount > 1 && mTool == Tool.ZOOM) {
//            println("SCALING NOW")
            mScaleGestureDetector.onTouchEvent(event)
        } else
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    touchStart(touchX, touchY)
                    invalidate()
                }
                MotionEvent.ACTION_MOVE -> {
                    touchMove(touchX, touchY)
                    invalidate()
                }
                MotionEvent.ACTION_UP -> {
                    touchUp()
                    invalidate()
                }
                else -> return false
            }
        return true
    }

    fun deleteAll() {
        println("deleteAll")
        drawPath = Path()

        mDrawPathsRedo.addAll(mDrawPaths.clone() as LinkedList<Path>)
        mDrawPaintsRedo.addAll(mDrawPaints.clone() as LinkedList<Paint>)
        mDrawPaints.clear()
        mDrawPaths.clear()
        invalidate()
    }


    private fun touchStart(x: Float, y: Float) {
        mDrawPathsRedo.clear()
        mDrawPaintsRedo.clear()
        /*originX = 0f
        originY = 0f*/
        println("touchStart")

        mX = getNotScaleX(x)
        mY = getNotScaleY(y)
        startX = mX
        startY = mY

        drawPath = Path()
        // if (mTool == Tool.BRUSH)
        //drawPath!!.moveTo(x, y)
        drawPath!!.moveTo(startX, startY)

        println(drawPath)
        if (mTool != Tool.ZOOM) {
            mDrawPaths.addLast(drawPath)
            mDrawPaints.addLast(drawPaint)
      }

    }

    private fun touchUp() {
    }

    private fun touchMove(x: Float, y: Float) {
        println("touchMove")
        val a = Math.min(getNotScaleX(x), mX)
        val b = Math.min(getNotScaleY(y), mY)
        val c = Math.max(getNotScaleX(x), mX)
        val d = Math.max(getNotScaleY(y), mY)

        val dx = Math.abs(getNotScaleX(x) - mX)
        val dy = Math.abs(getNotScaleY(y) - mY)

        if (dx >= sTouchTolerance || dy >= sTouchTolerance) {
            val p = Paint(drawPaint)
            p.strokeWidth = mCurrentBrushSize.toFloat()
            p.color = mCurrentColor

            when (mTool) {
                Tool.BRUSH -> {
                    drawPath!!.quadTo(mX, mY, (getNotScaleX(x) + mX) / 2, (mY + getNotScaleY(y)) / 2)
                    mX = getNotScaleX(x)
                    mY = getNotScaleY(y)
                }
                Tool.RECTANGLE -> {
                    drawPath = Path()

                    drawPath!!.addRect(a, b, c, d, Path.Direction.CW)

                }

                Tool.TRIANGLE -> {

                    drawPath = Path()
                    drawPath!!.moveTo(mX + dx / 2, mY)
                    drawPath!!.lineTo(getNotScaleX(x), getNotScaleY(y))
                    drawPath!!.lineTo(mX, getNotScaleY(y))

                    drawPath!!.lineTo(mX + dx / 2, mY)

                }
                Tool.OVAL -> {
                    drawPath = Path()
                    drawPath!!.addOval(a, b, c, d,
                            Path.Direction.CW)

                }
                Tool.ZOOM -> {

                    println("MOVE")
                    val dx1 = mX - getNotScaleX(x)
                    val dy1 = mY - getNotScaleY(y)
                    originX -= dx1 * mScaleFactor
                    originY -= dy1 * mScaleFactor

                    if (originX < width - width * mScaleFactor) {
                        originX = width - width * mScaleFactor
                    }
                    if (originY < height - height * mScaleFactor) {
                        originY = height - height * mScaleFactor
                    }
                    if (originX > 0) {
                        originX = 0f
                    }
                    if (originY > 0) {
                        originY = 0f
                    }
                }
                Tool.TEXT -> {

                }
            }

            /*  mX = getGlCo(x[, Axis.X)
              mY = getGlCo(y, Axis.Y)*/
if (mTool!=Tool.ZOOM) {
    mDrawPaths[mDrawPaths.size - 1] = drawPath!!
    mDrawPaints[mDrawPaints.size - 1] = p
}
        }
    }


    fun onClickUndo() {
        println("onClickUndo")
        if (mDrawPaths.size > 0) {
            println("onClickUndo and it woks")
            mDrawPathsRedo.addLast(mDrawPaths.last)
            println(mDrawPaths.removeLast())


            mDrawPaintsRedo.addLast(mDrawPaints.last)
            mDrawPaints.removeLast()


            invalidate()
        }
    }

    fun onClickRedo() {
        println("onClickRedo")
        if (mDrawPathsRedo.size > 0) {
            println("onClickRedo  and it woks")
            mDrawPaths.addLast(mDrawPathsRedo.last)
            println(mDrawPathsRedo.removeLast())


            mDrawPaints.addLast(mDrawPaintsRedo.last)
            mDrawPaintsRedo.removeLast()

            invalidate()

        }

    }

    fun setBrushSize(newSize: Int) {
        mCurrentBrushSize = newSize
    }

    fun setColor(color: Int) {
        mCurrentColor = color
    }

    fun setImage(image: Bitmap) {

        println("setImage")
        var setImage: Bitmap = image
        if (height > 0 && width > 0)
            setImage = Bitmap.createScaledBitmap(image, width, height, true)
        mCanvasBitmap = setImage
        mDrawCanvas = Canvas(mCanvasBitmap)

        println("mCanvasBitmap " + mCanvasBitmap)

        // clear old
        mDrawPaints.clear()
        mDrawPaintsRedo.clear()
        mDrawPathsRedo.clear()
        mDrawPaths.clear()
        invalidate()
    }

    fun getImage(): Bitmap? {
        var i = 0
        while (mDrawPaths.size > i) {
            val path = mDrawPaths[i]
            val paintLine = mDrawPaints[i]
            mDrawCanvas!!.drawPath(path, paintLine)

            i++
        }
        return mCanvasBitmap
    }

    private fun clearBitmap(): Bitmap {
        val bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888)
        bitmap.eraseColor(Color.WHITE)

        mDrawCanvas = Canvas(bitmap)
        return bitmap
    }

    fun getTool(): Tool = mTool
    fun setTool(tool: Tool) {
        mTool = tool
    }

    fun setText(text: String) {

        mText = text
    }

    inner class ScaleListener : ScaleGestureDetector.SimpleOnScaleGestureListener() {

        override fun onScale(detector: ScaleGestureDetector?): Boolean {

            detector?.let {
                val scale = mScaleFactor * it.scaleFactor
                mScaleFactor = maxOf(sMinScale, minOf(scale, sMaxScale))
            }


            val dx1 = mX - getNotScaleX(x)
            val dy1 = mY - getNotScaleY(y)
            originX -= dx1 * mScaleFactor
            originY -= dy1 * mScaleFactor
            invalidate()
            return true
        }

        override fun onScaleEnd(detector: ScaleGestureDetector?) {
            super.onScaleEnd(detector)

//            mTool = Tool.MOVE
        }
    }

    private fun getNotScaleX(coordinate: Float): Float = coordinate / (mScaleFactor) - (originX / mScaleFactor)


    private fun getNotScaleY(coordinate: Float): Float = coordinate / (mScaleFactor) - (originY / mScaleFactor)
}
