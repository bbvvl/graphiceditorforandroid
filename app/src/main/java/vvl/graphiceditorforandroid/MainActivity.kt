package vvl.graphiceditorforandroid

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Build
import android.os.Build.VERSION_CODES.LOLLIPOP
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.Settings
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import me.priyesh.chroma.ChromaDialog
import me.priyesh.chroma.ColorMode
import me.priyesh.chroma.ColorSelectListener
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val sKeyColor = "KEY_COLOR"
    private val sKeyImage = "KEY_IMAGE"
    private val sKeyTool = "KEY_TOOl"

    private val sWriteExternalStoragePermission = 0
    private val sReadExternalStoragePermission = 1
    private val sPermissionRequestCode = 2
    private val sRequestOpenImage = 3
    private val sRequestTakePhoto = 4

    private var mCurrentPhotoPath: String? = null
    private var mCurrentColor: Int = Color.BLACK
    private var mSize: Int = 3

    private lateinit var mFun: () -> Unit

    private lateinit var mPaintView: PaintView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        mPaintView = findViewById(R.id.paint_view)

    }

    override fun onResume() {
        super.onResume()

        println("onResume ")

        if (Intent.ACTION_VIEW == intent.action || Intent.ACTION_SEND == intent.action) {
            val imageUri = intent.data
            if (imageUri != null) {
                val path = imageUri.path

                val options = BitmapFactory.Options()
                options.inMutable = true

                val image = BitmapFactory.decodeFile(path, options)
                mPaintView.setImage(image)
            }
        }
        println("mCurrentPhotoPath " + mCurrentPhotoPath)
        if (mCurrentPhotoPath != null) {
            val o = BitmapFactory.Options()
            o.inMutable = true
            println("mCurrentPhotoPath " + mCurrentPhotoPath)

            val imageBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, o)
            println("imageBitmap " + imageBitmap)
            if (imageBitmap != null) mPaintView.setImage(imageBitmap)

        }


    }

    private fun setSettings(item: MenuItem) {

        when (item.itemId) {
            R.id.action_delete -> {
                mPaintView.deleteAll()
            }
            R.id.action_undo -> {
                mPaintView.onClickUndo()
            }
            R.id.action_redo -> {
                mPaintView.onClickRedo()
            }

            R.id.action_open_nav_view -> {

                val dl = findViewById<DrawerLayout>(R.id.draw_layout)

                if (!dl.isDrawerOpen(GravityCompat.END))
                    dl.openDrawer(GravityCompat.END)
                else dl.closeDrawer(GravityCompat.END)
            }
        }
    }

    private fun setDrawSettings(item: MenuItem) {

        when (item.itemId) {
            R.id.action_color -> {
                ChromaDialog.Builder()
                        .initialColor(mCurrentColor)
                        .colorMode(ColorMode.RGB)
                        .onColorSelected(
                                object : ColorSelectListener {
                                    override fun onColorSelected(color: Int) {
                                        mCurrentColor = color

                                        item.icon.setColorFilter(mCurrentColor, PorterDuff.Mode.SRC)
                                        mPaintView.setColor(mCurrentColor)

                                    }
                                })
                        .create()
                        .show(supportFragmentManager, "ChromaDialog")
            }

            R.id.action_size -> {

                val builder = AlertDialog.Builder(this)

                val view: View = layoutInflater.inflate(R.layout.alert_dialog_view_brush_size, null)
                builder.setView(view)
                val dialog: AlertDialog = builder.create()
                val seekBar = view.findViewById<SeekBar>(R.id.seek_bar)
                seekBar.max = 30
                seekBar.progress = mSize

                view.findViewById<TextView>(R.id.current_text_view).text = mSize.toString()

                seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                    override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                        view.findViewById<TextView>(R.id.current_text_view).text = i.toString()
                    }

                    override fun onStartTrackingTouch(seekBar: SeekBar) {

                    }

                    override fun onStopTrackingTouch(seekBar: SeekBar) {

                        mSize = seekBar.progress
                        if (mSize == 0) ++mSize
                        item.title = mSize.toString()

                        mPaintView.setBrushSize(mSize)
                        dialog.dismiss()
                    }

                })


                dialog.show()
            }
        }

    }

    override fun onBackPressed() {

        val mDL = findViewById<DrawerLayout>(R.id.draw_layout)

        if (mDL.isDrawerOpen(GravityCompat.END) || mDL.isDrawerOpen(GravityCompat.START)) {
            mDL.closeDrawers()

        } else super.onBackPressed()
    }

    override fun onNavigationItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_brush -> consume { onBackPressed(); println("action_brush");mPaintView.setTool(Tool.BRUSH) }
        R.id.action_circle -> consume {
            onBackPressed()
            if (android.os.Build.VERSION.SDK_INT >= LOLLIPOP) {
                println("action_circle");mPaintView.setTool(Tool.OVAL)
            } else {
                Toast.makeText(this, "You'r sdk version < 21", Toast.LENGTH_SHORT).show()
            }
        }
        R.id.action_rectangle -> consume { onBackPressed(); println("action_rectangle");mPaintView.setTool(Tool.RECTANGLE) }
        R.id.action_triangle -> consume { onBackPressed(); println("action_triangle");mPaintView.setTool(Tool.TRIANGLE) }
        R.id.action_zoom -> consume { onBackPressed(); println("action_zoom");mPaintView.setTool(Tool.ZOOM) }

        R.id.action_open -> consume { onBackPressed(); println("action_open"); checkAndRequestReadPermission() }
        R.id.action_photo -> consume { onBackPressed(); println("action_photo"); takePhoto() }
        R.id.action_share -> consume { onBackPressed(); println("action_share"); mFun = { this.share() }; checkAndRequestWritePermission() }
        R.id.action_text -> consume {
            onBackPressed(); println("action_text")
            val builder = AlertDialog.Builder(this)

            val view: View = layoutInflater.inflate(R.layout.alert_dialog_view_input_text, null)
            builder.setView(view)
            val ad = builder.create()
            view.findViewById<Button>(R.id.ok_button).setOnClickListener({
                mPaintView.setText(view.findViewById<EditText>(R.id.edit_text).text.toString())
                mPaintView.setTool(Tool.TEXT)
                ad.dismiss()
            })
            ad.show()
            ad.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        }
        R.id.action_save -> consume {

            mFun = { this.saveImage() }

            checkAndRequestWritePermission()

        }
        else -> consume { }

    }

    private fun openImage() {
        val i = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        startActivityForResult(i, sRequestOpenImage)
    }

    private fun share() {
        println("share")
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND

        val image: Bitmap = mPaintView.getImage()!!

        val file: File = createImageFile()
        image.compress(Bitmap.CompressFormat.JPEG, 100, FileOutputStream(file))

        val photoURI: Uri = FileProvider.getUriForFile(applicationContext,
                "vvl.graphiceditorforandroid.fileprovider",
                file)

        shareIntent.putExtra(Intent.EXTRA_STREAM, photoURI)
        shareIntent.type = "image/jpeg"
        startActivity(shareIntent)
    }

    private fun takePhoto() {

        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            // Create the File where the photo should go
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                // Error occurred while creating the File
                println("OU THIS IS ERROR")
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                val photoURI: Uri = FileProvider.getUriForFile(applicationContext,
                        "vvl.graphiceditorforandroid.fileprovider",
                        photoFile)

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, sRequestTakePhoto)
            }
        }
    }

    private fun checkAndRequestReadPermission() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= 23) {
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        sReadExternalStoragePermission)
            }
        } else {
            openImage()
        }
    }

    private fun checkAndRequestWritePermission() {
        println("checkAndRequestWritePermission")
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= 23) {
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        sWriteExternalStoragePermission)
            }
        } else {
            mFun()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>
                                            , grantResults: IntArray
    )
            =
            if (requestCode == sWriteExternalStoragePermission) {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mFun()
                } else {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        val builder = AlertDialog.Builder(this)

                        builder.setMessage(R.string.permission_w)

                        builder.setPositiveButton(android.R.string.ok
                        ) { _, _ ->
                            if (Build.VERSION.SDK_INT >= 23)
                                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                        sWriteExternalStoragePermission)
                        }

                        builder.setNegativeButton(android.R.string.cancel, null)
                        builder.create().show()

                    } else
                        showNoPermissionSnackBar()
                }
            } else
                if (requestCode == sReadExternalStoragePermission) {
                    if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        openImage()
                    } else {

                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                                Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            val builder = AlertDialog.Builder(this)

                            builder.setMessage(R.string.permission_r)

                            builder.setPositiveButton(android.R.string.ok
                            ) { _, _ ->
                                if (Build.VERSION.SDK_INT >= 23)
                                    requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                                            sReadExternalStoragePermission)
                            }

                            builder.setNegativeButton(android.R.string.cancel, null)
                            builder.create().show()

                        } else
                            showNoPermissionSnackBar()
                    }
                } else super.onRequestPermissionsResult(requestCode, permissions, grantResults)

    private fun showNoPermissionSnackBar() {
        Snackbar.make(findViewById(R.id.ConstraintLayout), resources.getString(R.string.snackbar_text), Snackbar.LENGTH_LONG)
                .setAction("Settings") {
                    openApplicationSettings()
                    Toast.makeText(applicationContext,
                            resources.getString(R.string.snackbar_settings_text),
                            Toast.LENGTH_SHORT)
                            .show()
                }
                .show()
    }

    private fun openApplicationSettings() {
        val appSettingsIntent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + packageName))
        startActivityForResult(appSettingsIntent, sPermissionRequestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == sRequestOpenImage && resultCode == Activity.RESULT_OK && null != data) {
            val selectedImage = data.data
            val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

            val cursor = contentResolver.query(selectedImage!!,
                    filePathColumn, null, null, null)
            cursor!!.moveToFirst()

            val columnIndex = cursor.getColumnIndex(filePathColumn[0])
            mCurrentPhotoPath = cursor.getString(columnIndex)

            cursor.close()
        }

        if (requestCode == sRequestTakePhoto && resultCode == Activity.RESULT_CANCELED) {

            mCurrentPhotoPath = null
        }

    }

    private inline fun consume(f: () -> Unit): Boolean {
        f()
        return true
    }

    private fun init() {

        val mToolbarBottom = findViewById<Toolbar>(R.id.toolbar_bottom)
        mToolbarBottom.inflateMenu(R.menu.menu_bottom)
        mToolbarBottom.setOnMenuItemClickListener { item ->
            setDrawSettings(item)
            true
        }

        val mToolbarTop = findViewById<Toolbar>(R.id.toolbar_top)
        mToolbarTop.inflateMenu(R.menu.menu_top)
        mToolbarTop.setTitle(R.string.app_name)
        mToolbarTop.setOnMenuItemClickListener { item ->
            setSettings(item)
            true
        }

        findViewById<NavigationView>(R.id.left_nav_view).setNavigationItemSelectedListener(this)
        val rnv = findViewById<NavigationView>(R.id.right_nav_view)
        rnv.setNavigationItemSelectedListener(this)

        val mDrawer = findViewById<DrawerLayout>(R.id.draw_layout)
        val mToggle = object : ActionBarDrawerToggle(
                this, mDrawer, mToolbarTop, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            override fun onDrawerClosed(v: View) {
                if (v != rnv) super.onDrawerClosed(v);
            }

            override fun onDrawerOpened(v: View) {
                if (v != rnv) super.onDrawerOpened(v);

            }

            override fun onDrawerSlide(drawerView: View?, slideOffset: Float) {
                if (drawerView != rnv) super.onDrawerSlide(drawerView, slideOffset)
            }
        }

        mDrawer.addDrawerListener(mToggle)

        mToggle.syncState()
    }

    @Throws(IOException::class)
    private fun createImageFile()
            : File {

        val timeStamp = SimpleDateFormat.getDateTimeInstance(SimpleDateFormat.SHORT, SimpleDateFormat.SHORT).format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(imageFileName, ".jpg", storageDir)
        mCurrentPhotoPath = image.absolutePath
        return image

    }

    private fun saveImage() {
        println("saveImage")
        val timeStamp = SimpleDateFormat.getDateTimeInstance(SimpleDateFormat.SHORT, SimpleDateFormat.SHORT).format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"

        val myDir = File.createTempFile(imageFileName, ".jpg", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES))
        if (mPaintView.getImage()!!.compress(Bitmap.CompressFormat.JPEG, 100, FileOutputStream(myDir)))
            Toast.makeText(this, R.string.toask_save_complete, Toast.LENGTH_SHORT).show()
        else Toast.makeText(this, R.string.toask_save_incomplete, Toast.LENGTH_SHORT).show()
    }


    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        println("onRestoreInstanceState ")
        val color = savedInstanceState.getInt(sKeyColor)
        mPaintView.setColor(color)
        mCurrentColor = color

        mCurrentPhotoPath = savedInstanceState.getString(sKeyImage)
        /*val tool = savedInstanceState.getInt(sk)
        mPaintView.setTool(tool)*/


    }

    public override fun onSaveInstanceState(savedInstanceState: Bundle?) {
        super.onSaveInstanceState(savedInstanceState)

        println("onSaveInstanceState ")
        savedInstanceState!!.putInt(sKeyColor, mCurrentColor)
        //savedInstanceState.putParcelable(sKeyTool, mPaintView.getTool())
        savedInstanceState.putString(sKeyImage, mCurrentPhotoPath)

    }

}